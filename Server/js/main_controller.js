/*jslint undef:true*/
var mainApp = angular.module('mainApp');
mainApp.controller('mainController', ['$scope', '$http', 'cameraDataService', 'socketService', function ($scope, $http, cameraDataService, socketService) {
    'use strict';
    /*jslint undef:true*/
    $scope.buttonsInfo = [{"name":"SYSTEM","file":"js/sample.xml"},{"name":"APP","file":"js/sampleApp.xml"}];
    $scope.cameraDataService = cameraDataService;
    $scope.readXml = function () {
      // console.log(check);
      $http.get($scope.xmlName, {
        transformResponse: function (cnv) {
          var x2js = new X2JS(),
              aftCnv = x2js.xml_str2json(cnv);
          return aftCnv;
        }
      })
      .success(function (response) {
        $scope.data = response;
      });
    }
    $scope.xmlName = "js/sample.xml";
    $scope.readXml();
    $scope.showModal = false;
    $scope.open = function () {
        $scope.showModal = !$scope.showModal;
    };
    $scope.openFile = function(fileName) {
      $scope.xmlName = fileName;
      $scope.readXml();
    }
    $scope.update = function () {
        var js2x = new X2JS(),
            afttransform = new XMLSerializer().serializeToString(js2x.json2xml($scope.data)),
            config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };
        $scope.postRequest = function () {
            return $http.post('/xmldata', $.param({'data': afttransform, 'fileName': $scope.xmlName}), config);
        }
        $scope.postRequest()
        .success (function (data) {
          $scope.readXml();
        });
    };
}]);
mainApp.directive('stringToNumber', function () {
    "use strict";
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    };
});
mainApp.directive("ngMin", function () {
    "use strict";
    /*jslint undef:true*/
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModal) {
            var view_value;
            ngModal.$parsers.push(function (value) {
                var return_value,
                    min = scope.$eval(attrs.ngMin);
                if (value < min) {
                    return_value = min;
                    view_value = min;
                    ngModal.$setViewValue(view_value);
                    ngModal.$render();
                } else {
                    return_value = value;
                    view_value = return_value;
                }
                return '' + return_value;
            });
        }
    };
});
mainApp.directive('ngMax', function () {
    "use strict";
    /*jslint undef:true*/
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ngModel) {
            var view_value;
            ngModel.$parsers.push(function (value) {
                var return_value,
                    max = scope.$eval(attr.ngMax);
                if (value > max) {
                    return_value = max;
                    view_value = max;
                    ngModel.$setViewValue(view_value);
                    ngModel.$render();
                } else {
                    return_value = value;
                    view_value = return_value;
                }
                return '' + return_value;
            });
        }
    };
});
mainApp.directive('ngVal', function () {
  "use strict";
  /*jslint undef:true*/
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attr, ngModel) {
      var view_value;
      ngModel.$parsers.push(function (value) {
        var return_value,
            default_ip = scope.$eval(attr.ngVal);
            if(value.match(/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/)) {
              return_value = value;
              view_value = return_value;
            } else {
              return_value = default_ip;
              view_value = default_ip;
              ngModel.$setViewValue(view_value);
              ngModel.$render();
            }
            return return_value;
      });
    }
  };
});
mainApp.$inject = ['$scope'];
